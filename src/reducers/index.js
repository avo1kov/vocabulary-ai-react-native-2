import wordsSet from './wordsSet'
import { combineReducers } from 'redux'

export default combineReducers({ wordsSet })