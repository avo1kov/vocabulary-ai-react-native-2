const defaultState = [
    {
        word: 'enhanced',
        definition: 'улучшенный'
    },
    {
        word: 'conceived',
        definition: 'задумано'
    },
    {
        word: 'flawless',
        definition: 'безупречный'
    },
    {
        word: 'representative',
        definition: 'представитель'
    },
    {
        word: 'modwheel',
        definition: 'колесо модуляции'
    },
    {
        word: 'shorthand',
        definition: 'стенография'
    },
    {
        word: 'overnight',
        definition: 'заночевать'
    },
    {
        word: 'fiddle',
        definition: 'скрипка'
    },
    {
        word: 'covering',
        definition: 'покрытие'
    },
    {
        word: 'envelope',
        definition: 'конверт'
    },
    {
        word: 'persistent',
        definition: 'настойчивый'
    },
    {
        word: 'knob',
        definition: 'ручка'
    },
    {
        word: 'chapel',
        definition: 'часовня'
    },
    {
        word: 'routines',
        definition: 'заведенный порядок'
    },
    {
        word: 'redundant',
        definition: 'избыточный'
    },
    {
        word: 'treated',
        definition: 'обработанный'
    },
    {
        word: 'hissing',
        definition: 'шипение'
    },
    {
        word: 'fade out',
        definition: 'постепенно ослабевать'
    },
    {
        word: 'amplifier',
        definition: 'усилитель'
    },
    {
        word: 'rife',
        definition: 'распространённый'
    },
    {
        word: 'superstition',
        definition: 'суеверие'
    },
    {
        word: 'outfit',
        definition: 'снаряжение'
    },
    {
        word: 'directly',
        definition: 'непосредственно'
    },
    {
        word: 'performance',
        definition: 'спектакль'
    },
    {
        word: 'particular',
        definition: 'определенный, особый​'
    },
    {
        word: 'straight',
        definition: 'прямой'
    },
    {
        word: 'drums',
        definition: 'барабаны'
    },
    {
        word: 'accustomed',
        definition: 'привыкший'
    },
    {
        word: 'slightly',
        definition: 'слегка'
    },
    {
        word: 'pipe',
        definition: 'труба'
    },
    {
        word: 'wisdom',
        definition: 'мудрость'
    },
    {
        word: 'snippets',
        definition: 'фрагменты'
    },
    {
        word: 'abandon',
        definition: 'оставить, отказаться'
    },
    {
        word: 'hungry',
        definition: 'голодный'
    },
    {
        word: 'hiring',
        definition: 'найм'
    },
    {
        word: 'rest',
        definition: 'остальное'
    },
    {
        word: 'reportedly',
        definition: 'по сообщениям'
    },
    {
        word: 'obsessive',
        definition: 'навязчивый'
    },
    {
        word: 'appears',
        definition: 'кажется'
    },
    {
        word: 'succeed',
        definition: 'преуспевать'
    },
    {
        word: 'obsessively',
        definition: 'одержимо'
    },
    {
        word: 'youth',
        definition: 'молодежь'
    },
    {
        word: 'devote',
        definition: 'посвятить'
    },
    {
        word: 'comforting',
        definition: 'утешительный'
    },
    {
        word: 'otherwise',
        definition: 'иначе'
    },
    {
        word: 'inherit',
        definition: 'наследовать'
    },
    {
        word: 'kinda',
        definition: 'вроде'
    },
    {
        word: 'voyage',
        definition: 'путешествие'
    },
    {
        word: 'preset',
        definition: 'предварительная установка'
    },
    {
        word: 'bypass',
        definition: 'обходить, пропустить'
    },
    {
        word: 'mirror',
        definition: 'зеркало'
    },
    {
        word: 'набрать вес',
        definition: 'gain weight'
    },
    {
        word: 'strength',
        definition: 'сила, мощь'
    },
    {
        word: 'fault',
        definition: 'ошибка'
    },
    {
        word: 'relieved',
        definition: 'облегченный'
    },
    {
        word: 'demanding',
        definition: 'требующий'
    },
    {
        word: 'chase',
        definition: 'погоня'
    },
    {
        word: 'desperate',
        definition: 'отчаянный'
    },
    {
        word: 'struggling',
        definition: 'борющийся'
    },
    {
        word: 'muddy',
        definition: 'мутный'
    },
    {
        word: 'bloke',
        definition: 'парень, мужик, тип'
    },
    {
        word: 'curing',
        definition: 'отверждение'
    },
    {
        word: 'healing energy',
        definition: 'целительная энергия'
    },
    {
        word: 'urge',
        definition: 'побуждать'
    }
]

const wordsSet = (state = shuffle(defaultState), action) => {
    switch (action.type) {
        case 'REPLY_YES':
            // console.log('replied', state.slice(1, state.length));
            return [ ...state.slice(1, state.length) ]
        case 'REPLY_NO':
            return state.length ? [
                state[1],
                ...shuffle([
                    ...state.slice(2, state.length),
                    state[0],
                    state[0]
                ])
            ] : []
        case 'SET_WORDS':
            return action.payload

        default:
            return state
    }
  }
  
  export default wordsSet

function shuffle(a) {
    for (let i = a.length - 1; i > 0; i--) {
        const j = Math.floor(Math.random() * (i + 1));
        [a[i], a[j]] = [a[j], a[i]];
    }
    return a;
}