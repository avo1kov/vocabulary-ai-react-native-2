export const replyYes = word => ({
    type: 'REPLY_YES',
    word
})

export const replyNo = word => ({
    type: 'REPLY_NO',
    word
})

export const setWords = payload => ({
    type: 'SET_WORDS',
    payload
})