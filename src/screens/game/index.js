import React from 'react'
import { StatusBar } from 'react-native'
import { Wrapper, DoYouKnow, ScrollWrapper, Card, Word, DefinitionWrapper, DefinitionText, wordHeight, Yes, No, NextWordWrapper, NextWordText } from './styles'
import * as Haptics from 'expo-haptics'
import { connect } from 'react-redux'
import { replyYes, replyNo, setWords } from '../../actions'

class GameScreen extends React.Component {
    static navigationOptions = ({ navigation }) => ({
        title: navigation.getParam('title', '')
    });

    constructor(props) {
        super(props);
        this.state = {
            isDefitionShowing: false,
            isRepliedYes: false,
            isRepliedNo: false,
            scrollY: 0,
            scrollYStart: 0,
            isFinished: false,
        };
    }

    componentDidMount = () => {
        const { navigation } = this.props;
        const lexemes = navigation.getParam('lexemes', {});
        let wordsSet = [];
        console.log(lexemes);
        lexemes.map(lexeme => {
            wordsSet.push({
                word: lexeme.token,
                definition: lexeme.definition
            });
        });
        this.props.setWords(wordsSet);
    }

    handleScroll = (event) => {
        if (this.props.wordsSet.length > 0) {
            this.setState({
                scrollY: event.nativeEvent.contentOffset.y
            });
            if (event.nativeEvent.contentOffset.y > 0 && !this.state.isDefitionShowing && !this.state.willDefitionShowing) {
                this.setState({ willDefitionShowing: true }, () => {
                    Haptics.impactAsync(Haptics.ImpactFeedbackStyle.Light);
                });
            }
            if (event.nativeEvent.contentOffset.y <= 0 && !this.state.isDefitionShowing && this.state.willDefitionShowing) {
                this.setState({ willDefitionShowing: false }, () => {
                    Haptics.impactAsync(Haptics.ImpactFeedbackStyle.Light);
                });
            }

            if (this.scrollYStart - event.nativeEvent.contentOffset.y < 40 && this.state.isDefitionShowing && this.state.willDefitionShowing) {
                this.setState({ willDefitionShowing: false }, () => {
                    Haptics.impactAsync(Haptics.ImpactFeedbackStyle.Light);
                });
            }
            if (this.scrollYStart - event.nativeEvent.contentOffset.y >= 40 && this.state.isDefitionShowing && !this.state.willDefitionShowing) {
                this.setState({ willDefitionShowing: true }, () => {
                    Haptics.impactAsync(Haptics.ImpactFeedbackStyle.Light);
                });
            }

            if (event.nativeEvent.contentOffset.y <= -30 && !this.state.isRepliedYes) {
                Haptics.impactAsync(Haptics.ImpactFeedbackStyle.Heavy);
                this.setState({ isRepliedYes: true });
            }
            if (event.nativeEvent.contentOffset.y > -30 && this.state.isRepliedYes) {
                Haptics.impactAsync(Haptics.ImpactFeedbackStyle.Heavy);
                this.setState({ isRepliedYes: false });
            }
            if (event.nativeEvent.contentOffset.y > 120 && !this.state.isRepliedNo) {
                Haptics.impactAsync(Haptics.ImpactFeedbackStyle.Heavy);
                this.setState({ isRepliedNo: true });
            }
            if (event.nativeEvent.contentOffset.y <= 120 && this.state.isRepliedNo) {
                Haptics.impactAsync(Haptics.ImpactFeedbackStyle.Heavy);
                this.setState({ isRepliedNo: false });
            }
            if (event.nativeEvent.contentOffset.y == 0) {
                this.setState({
                    isRepliedYes: false,
                    isRepliedNo: false,
                });
            }
        }
    }

    handleOnScrollBeginDrag = (event) => {
        this.setState({
            scrollYStart: event.nativeEvent.contentOffset.y
        });
    }

    handleOnScrollEndDrag = () => {
        this.setState({
            scrollY: 0
        });
        if (this.props.wordsSet.length > 0) {
            if (this.state.isRepliedYes) {
                this.props.replyYes({ word: 'cat', definition: 'kitten'});
                this.scrollView.scrollTo({y: 0, animated: false});
            } else {
                if (this.state.isRepliedNo) {
                    this.scrollView.scrollTo({y: -200, animated: true});
                    this.props.replyNo({ word: 'cat', definition: 'kitten'});
                } else {
                    if (this.state.willDefitionShowing) {
                        this.setState({ isDefitionShowing: true, willDefitionShowing: false });
                        this.scrollView.scrollToEnd({animated: true});
                    } else {
                        this.scrollView.scrollTo({y: 0, animated: true});
                        this.setState({ isDefitionShowrkavjing: false, willDefitionShowing: false });
                    }
                }
            }
        } else {
            this.setState({
                isDefitionShowing: false,
                willDefitionShowing: false,
                isRepliedYes: false,
                isRepliedNo: false
            });
        }
    }

    render() {
        return (
            <Wrapper>
                <StatusBar barStyle='dark-content' />
                { this.props.wordsSet.length > 0 ? 
                    <DoYouKnow>
                        Do you know? {this.state.isRepliedYes ? <Yes>Yes</Yes> : this.state.isRepliedNo ? <No>No</No> : null}
                    </DoYouKnow>
                : null }
                { this.props.wordsSet.length > 0 ?
                    <NextWordWrapper scrollY={this.state.scrollY}>
                        <NextWordText wordLength={(this.props.wordsSet.length > 1 ? this.props.wordsSet[1].word : 'It is all').length}>
                            {
                                this.props.wordsSet.length > 1
                                    ? this.props.wordsSet[1].word
                                    : null
                            }
                        </NextWordText>
                    </NextWordWrapper>
                : null }
                <ScrollWrapper
                    ref={ref => this.scrollView = ref}
                    scrollEventThrottle={16}
                    onScroll={this.handleScroll}
                    onScrollBeginDrag={this.handleOnScrollBeginDrag}
                    onScrollEndDrag={this.handleOnScrollEndDrag}
                    showsVerticalScrollIndicator={false}
                >
                    <Card>
                        <Word wordLength={(this.props.wordsSet.length ? this.props.wordsSet[0].word : 'It is all').length}>
                            { this.props.wordsSet.length ? this.props.wordsSet[0].word : 'It is all' }
                        </Word>
                        <DefinitionWrapper>
                            <DefinitionText>
                                { this.props.wordsSet.length ? this.props.wordsSet[0].definition : null }
                            </DefinitionText>
                        </DefinitionWrapper>
                    </Card>
                </ScrollWrapper>
            </Wrapper>
        )
    }
}


const mapStateToProps = state => ({
    wordsSet: state
})
  
const mapDispatchToProps = dispatch => ({
    replyYes: word => dispatch(replyYes(word)),
    replyNo: word => dispatch(replyNo(word)),
    setWords: words => dispatch(setWords(words)),
})

export default connect(
        mapStateToProps,
        mapDispatchToProps
    )(GameScreen)