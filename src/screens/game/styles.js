import styled from 'styled-components/native'
import { getStatusBarHeight } from 'react-native-status-bar-height'
import { Dimensions } from 'react-native'
import Constants from 'expo-constants'

const screenWidth = Math.round(Dimensions.get('window').width);
const screenHeight = Math.round(Dimensions.get('window').height);
export const wordHeight = 130;
const statusBarHeight = getStatusBarHeight();

const isiPhoneX = Platform.OS == 'ios' && (
  Constants.platform.ios.model.toLowerCase().includes('iphone x') ||
  Constants.deviceName.toLowerCase().includes('iphone x') ||
  Constants.platform.ios.model.toLowerCase().includes('iphone 11') ||
  Constants.deviceName.toLowerCase().includes('iphone 11')
)
const footerSpaceHeight = isiPhoneX ? 34 : 0

export const Wrapper = styled.View`
    z-index: 1;
    position: absolute;
    flex: 1;
    width: 100%;
`;

export const DoYouKnow = styled.Text`
    z-index: 2;
    position: absolute;
    top: 0;
    width: 100%;
    font-size: 20px;
    text-align: center;
    padding-top: ${screenHeight - wordHeight - 300}px;
    /* background: blue; */
`;

export const ScrollWrapper = styled.ScrollView`
    z-index: 3;
    position: absolute;
    top: 0;
    width: 100%;
    height: ${screenHeight - footerSpaceHeight - 64}px;
    overflow: visible;
    /* height: 100%; */
    /* flex: 1; */
    /* background: green; */
    /* background: rgba(200, 100, 150, 0.3); */
`;

export const NextWordWrapper = styled.View`
    margin: ${screenHeight - wordHeight}px 10px 0;
    padding-left: 20px;
    padding-top: 10px;
    height: ${wordHeight}px;
    /* box-shadow: 0 0 7px rgba(0, 0, 0, ${props => -props.scrollY / wordHeight * 0.2}); */
    transform: translateY(-10px) scale(0.95);
    opacity: 0.3;
    border-radius: 10px;
    background: white;
    border: solid gray 2px;
`;

export const NextWordText = styled.Text`
    font-size: ${props => 52 - props.wordLength / 20 * 10 || 52}px;
`;

export const Card = styled.View`
    margin: ${screenHeight - wordHeight - footerSpaceHeight - 90}px 10px 19px;
    background: white;
    box-shadow: 0 0 7px rgba(0, 0, 0, 0.2);
    border: solid gray 2px;
    border-radius: 10px;
    padding-top: 10px;
    /* overflow: hidden; */
    /* padding: 0 20px; */
`;

export const Word = styled.Text`
    /* color: white; */
    /* text-align: center; */
    font-size: ${props => 52 - props.wordLength / 20 * 10 || 52}px;
    height: ${wordHeight}px;
    padding: 0 20px;
    /* background: #fcdbff; */
`;

export const DefinitionWrapper = styled.View`
    background: rgba(0, 0, 0, 0.05);
    padding: 20px 20px;
    border-bottom-left-radius: 10px;
    border-bottom-right-radius: 10px;
`;

export const DefinitionText = styled.Text`
    font-size: 20px;
`;

export const Yes = styled.Text`
    color: green;
    font-style: italic;
`;

export const No = styled.Text`
    color: red;
    font-style: italic;
`;
