import React, { useState, useEffect } from 'react'
import { AsyncStorage, Animated, StatusBar, View, Text, Image, FlatList } from 'react-native'
import { MySafeArea, CollectionsScroll, CollectionsWrapper, CollectionTouchableWrapper, CollectionItem, CollectionEmoji, CollectionName, WaitForLoad, CollectionCreationWrapper, CollectionCreationLabel, CollectionsLabel, CreationButtons, CreationButton, CreationButtonEmoji, CreationButtonLabel, RatingView, Brain, TouchableWordsCountInteger, WordsCountInteger, WordsCountLabel, TouchableRatingNumber, RatingNumber, RatingLabel, RatingWordsInfo, RatingNumberInfo, SwipePanel, SwipeDeleteButton, SwipeEditButton, SwipeDeleteButtonText, SwipeEditButtonText, SwipeButtonImage } from './styles'
import { SERVER_URL } from 'react-native-dotenv'
import Swipeable from 'react-native-gesture-handler/Swipeable';

class CollectionsScreen extends React.Component {
    static navigationOptions = () => {
        return {
            header: null,
        };
    };
    constructor(props) { 
        super(props);
        this.state = {
            colors: ['#00a2ff', '#ff477e', '#62d175', '#af52de', 'black', '#ff9500', '#8e8e93'],
            collections: [],
            loaded: false,
            editable: false,
            collectionItemsOpacity: new Animated.Value(0),
            collectionItemsRotationAngle: new Animated.Value(0),
        };
    }
    getToken = async () => {
        try {
          const value = await AsyncStorage.getItem('@token')
          if(value !== null) {
            // value previously stored
            return value;
          }
          return null;
        } catch {
        }
    }
    refresh = () => {
        console.log("Refreshing");
        this.getToken().then(token => {
            fetch(`${SERVER_URL}/api/get_collections/`, {
                method: 'GET',
                headers: {
                    Authorization: token
                }
            })
                .then((response) => response.json())
                .then((responseJson) => {
                    const collections = responseJson.collections;
                    this.setState({ collections: [] }, () => {
                        this.setState({ collections: collections, loaded: true }); 
                    });
                    Animated.timing(this.state.collectionItemsOpacity, {
                        toValue: 1,
                        duration: 300,
                    }).start();
                })
                .catch(() => {
                    console.log("Error", `${SERVER_URL}/api/get_collections/`);
                });
        });
    }
    componentDidMount() {
        this.refresh();
        this.focusListener = this.props.navigation.addListener('didFocus', () => {
            console.log('do refresh!');
            this.refresh();
            
        });
    }
    componentWillUnmount() {
        // Remove the event listener before removing the screen from the stack
        this.focusListener.remove();
    }
    setEditableState = () => {
        this.props.navigation.setParams({ editable: !this.state.editable });
        if (!this.state.editable) {
            this.setState(prev => ({ editable: !prev.editable, collectionItemsRotationAngle: new Animated.Value(-1) }), () => {
                Animated.spring(this.state.collectionItemsRotationAngle, {
                    toValue: 1,
                    duration: 2000,
                    damping: 0.001,
                    stiffness: 150,
                }).start();
            });
        } else {
            this.setState(prev => ({ editable: !prev.editable, collectionItemsRotationAngle: new Animated.Value(0) }));
        }
    }
    openFileDialog = () => {
    }
    openManualCreationScreen = () => {
        this.props.navigation.navigate('CreateCollection');
    }
    deleteCollectionById = id => {
        const collections = this.state.collections.filter(collection => collection.id != id);
        this.setState({
            collections: [],
        }, () => {
            this.setState({ collections: collections });
        });
        this.getToken().then(token => {
            fetch(`${SERVER_URL}/api/delete_collection/`, {
                method: 'DELETE',
                headers: {
                    Authorization: token,
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({ id })
            })
                .then((response) => response.json())
                .catch(() => {
                    console.log("Error", `${SERVER_URL}/api/get_collections/`);
                });
        });
    }
    editCollection = collection => {
        this.props.navigation.navigate('EditCollection', { collectionData: {
                id: collection.id,
                emoji: collection.emoji,
                name: collection.name,
                words: collection.words
            }
        })
    }
    leftActions = collection => (
        <SwipePanel>
            <SwipeDeleteButton onPress={() => {this.deleteCollectionById(collection.id)}}>
                    <SwipeButtonImage
                        source={require('../../images/bin.svg')}
                    />
            </SwipeDeleteButton>
            <SwipeEditButton onPress={() => {this.editCollection(collection)}}>
                    <SwipeButtonImage
                        source={require('../../images/pen.svg')}
                    />
            </SwipeEditButton>
        </SwipePanel>
    )
    render() {
        const {navigate} = this.props.navigation;
        return (
            <MySafeArea>
                <CollectionsScroll>
                    <CollectionCreationWrapper>
                        <CollectionCreationLabel>Создать набор слов:</CollectionCreationLabel>
                        <CreationButtons>
                            <CreationButton onPress={() => {this.openFileDialog()}}>
                                <CreationButtonEmoji>📑</CreationButtonEmoji>
                                <CreationButtonLabel>Из файла</CreationButtonLabel>
                            </CreationButton>
                            <CreationButton>
                                <CreationButtonEmoji>🔗</CreationButtonEmoji>
                                <CreationButtonLabel>По ссылке</CreationButtonLabel>
                            </CreationButton>
                            <CreationButton onPress={this.openManualCreationScreen}>
                                <CreationButtonEmoji>👏</CreationButtonEmoji>
                                <CreationButtonLabel>Руками</CreationButtonLabel>
                            </CreationButton>
                        </CreationButtons>
                    </CollectionCreationWrapper>
                    <CollectionsWrapper style={!this.state.loaded ? { justifyContent: 'center' } : null}>
                    <CollectionsLabel>Ваши наборы слов:</CollectionsLabel>
                    {   this.state.loaded ?
                        this.state.collections.map((collection, index) => 
                                <CollectionTouchableWrapper
                                    onPress={() => 
                                        navigate('GameSwiper', {
                                            title: collection.name,
                                            lexemes: collection.words
                                        })
                                    }
                                    disabled={this.state.editable}
                                    key={index}
                                >
                                    <Swipeable renderRightActions={() => this.leftActions(collection)} rightThreshold={50} key={index}>
                                        <BaseLine>
                                            <CollectionItem>
                                                <CollectionEmoji>{collection.emoji}</CollectionEmoji>
                                                <CollectionName>{collection.name}</CollectionName>
                                            </CollectionItem>
                                        </BaseLine>
                                </Swipeable>
                            </CollectionTouchableWrapper>
                        ) : <WaitForLoad>Wait for loading...</WaitForLoad>
                    }
                    </CollectionsWrapper>
                </CollectionsScroll>
            </MySafeArea>
        );
    }
}

export default CollectionsScreen