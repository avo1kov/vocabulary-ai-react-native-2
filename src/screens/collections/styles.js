import styled from 'styled-components/native'
import { Animated } from 'react-native'
import { getStatusBarHeight } from 'react-native-status-bar-height'

export const MySafeArea = styled.SafeAreaView`
    flex: 1;
`

export const CollectionsScroll = styled.ScrollView.attrs({
    contentContainerStyle: {
        flexDirection: 'column',
        marginTop: 12,
    }
})``

export const CollectionsWrapper = styled.View`
        flex-direction: column;
        justify-content: space-between;
        flex-wrap: wrap;
        margin: 10px 5px;
        /* padding: 0 10px; */
`

export const CollectionsLabel = styled.Text`
    text-align: center;
    font-size: 18px;
    font-weight: bold;
    padding-top: 15px;
    padding-bottom: 20px;
`

export const CollectionTouchableWrapper = styled.TouchableOpacity`
    width: 100%;
    background-color: white;
`

export const CollectionItem = Animated.createAnimatedComponent(styled.View`
    flex: 1;
    flex-direction: row;
    margin-bottom: 12px;
    padding-left: 20px;
    justify-content: flex-start;
    background-color: white;
    height: 45px;
    display: flex;
`)


export const CollectionName = styled.Text`
    font-size: 34pt;
    line-height: 36pt;
    font-weight: 600;
`

export const CollectionEmoji = styled.Text`
    font-size: 34px;
    font-weight: bold;
    width: 50px;
`

export const WaitForLoad = styled.Text`
    font-size: 25px;
`

export const CollectionCreationWrapper = styled.View`

`

export const CollectionCreationLabel = styled.Text`
    text-align: center;
    font-size: 18px;
    font-weight: bold;
`

export const CreationButtons = styled.View`
    flex-direction: row;
    justify-content: space-evenly;
    padding: 20px 0;
`
export const CreationButton = styled.TouchableOpacity`
    align-items: center;
`
export const CreationButtonEmoji = styled.Text`
    font-size: 38px;
`
export const CreationButtonLabel = styled.Text`
    padding: 8px 0;
`

const ratingFontSize = '20px';
export const RatingView = styled.View`
    padding: 0 10px 12px;
    flex-direction: row;
    justify-content: space-between;
`
export const RatingWordsInfo = styled.View`
    flex-direction: row;
`
export const RatingNumberInfo = styled.View`
    flex-direction: row;
`
export const Brain = styled.Text`
    font-size: ${ratingFontSize};
`
export const TouchableWordsCountInteger = styled.TouchableOpacity``
export const WordsCountInteger = styled.Text`
    font-size: ${ratingFontSize};
    padding: 0 4px;
    color: #af52de;
`
export const WordsCountLabel = styled.Text`
    font-size: ${ratingFontSize};
`
export const TouchableRatingNumber = styled.TouchableOpacity``
export const RatingNumber = styled.Text`
    font-size: ${ratingFontSize};
    padding-right: 4px;
    color: #af52de;
`
export const RatingLabel = styled.Text`
    font-size: ${ratingFontSize};
`

export const SwipePanel = styled.View`
    /* flex: 1; */
    flex-direction: row;
    justify-content: flex-end;
    /* background-color: black; */
    height: 45px;
`
export const SwipeDeleteButton = styled.TouchableOpacity`
    background-color: #ff453a;
    justify-content: center;
    padding: 0 20px;
`
export const SwipeDeleteButtonText = styled.Text`
    color: white;
    padding: 0 10px;
`
export const SwipeButtonImage = styled.Image`
    width: 30px;
    height: 30px;
`
export const SwipeEditButton = styled.TouchableOpacity`
    background-color: #0a84ff;
    justify-content: center;
    padding: 0 20px;
`
export const SwipeEditButtonText = styled.Text`
    color: white;
    padding: 0 10px;
`