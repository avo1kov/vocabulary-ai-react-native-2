import React, { useState, useEffect } from 'react'
import { AsyncStorage, Fragment } from 'react-native'
import { SERVER_URL } from 'react-native-dotenv'

class StartScreen extends React.Component {
    constructor(props) {
      super(props);
    }

    getToken = async () => {
      try {
        const value = await AsyncStorage.getItem('@token')
        if(value !== null) {
          // value previously stored
          return value;
        }
        return null;
      } catch(e) {
        // error reading value
      }
    }

    componentDidMount = () => {
      this.getToken().then(token => {
        console.log(token);
        if (token) {
          this.props.navigation.navigate('Collections');
        } else {
          this.props.navigation.navigate('AuthScreen');
        }
      });
    }
    
    render() {
        return (
            <React.Fragment></React.Fragment>
        );
    }
}

export default StartScreen