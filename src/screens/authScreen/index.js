import React, { useState, useEffect } from 'react'
import { AsyncStorage, Animated, StatusBar, Text } from 'react-native'
import { MySafeArea, Logo, Ai, AuthWrapper, AuthLabel, LoginInput, LoginButton, LoginButtonText } from './styles'
import { SERVER_URL } from 'react-native-dotenv'

class CollectionsScreen extends React.Component {
    constructor(props) {
      super(props);
      this.state = {
        username: '',
        password: ''
      }
    }

    getToken = async () => {
      try {
        const value = await AsyncStorage.getItem('@token')
        if(value !== null) {
          // value previously stored
          return value;
        }
      } catch(e) {
        // error reading value
      }
    }

    storeToken = async (value) => {
      try {
        await AsyncStorage.setItem('@token', value)
      } catch (e) {
        // saving error
      }
    }
    
    login = () => {
      fetch(`${SERVER_URL}/api/signin/`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({
          username: this.state.username,
          password: this.state.password
        })
      })
            .then((response) => response.json())
            .then((responseJson) => {
              if (responseJson.authenticated) {
                console.log("Success", responseJson.token);
                this.storeToken(responseJson.token);
                this.props.navigation.navigate('Collections');
              } else {
                // Wrong login or password
                console.log("Failed");
              }
            })
            .catch((error) => {
                console.log("Error", `${SERVER_URL}/api/signin/`, error);
            });
    }
    
    render() {
        return (
            <MySafeArea>
              <StatusBar barStyle='dark-content' />
              <AuthWrapper>
                  <Logo>Vocabulary.<Ai>AI</Ai></Logo>
                  <AuthLabel>E-mail или логин</AuthLabel>
                  <LoginInput
                    onChangeText={text => this.setState(() => ({ username: text }))}
                    autoCapitalize='none'
                  ></LoginInput>
                  <AuthLabel style={{paddingTop: 20}}>Пароль</AuthLabel>
                  <BaseLayout>
                    <LoginInput
                      secureTextEntry={true}
                      onChangeText={text => this.setState(() => ({ password: text }))}
                    ></LoginInput>
                    <LoginButton onPress={this.login}>
                      <LoginButtonText>Войти</LoginButtonText>
                    </LoginButton>
                  </BaseLayout>
              </AuthWrapper>
            </MySafeArea>
        );
    }
}

export default CollectionsScreen