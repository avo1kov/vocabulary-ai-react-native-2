import styled from 'styled-components/native'

export const MySafeArea = styled.SafeAreaView`
    flex: 1;
`

export const Logo = styled.Text`
    font-size: 32px;
    font-weight: bold;
    text-align: center;
    margin-bottom: 35px;
`
export const Ai = styled.Text`
    color: #af52de;
`

export const AuthWrapper = styled.View`
    margin: 0 30px;
    flex: 1;
    flex-direction: column;
    justify-content: flex-start;
    align-items: center;
    padding-top: 40px;
    /* background: grey; */
`
export const AuthLabel = styled.Text`
    text-align: left;
    font-size: 18px;
    font-weight: bold;
    width: 100%;
`
export const LoginInput = styled.TextInput`
    background-color: #e5e5ea;
    margin: 5px 0px;
    font-size: 24px;
    padding: 10px;
    border-radius: 10px;
    width: 100%;
`

export const LoginButton = styled.TouchableOpacity`
    width: 100%;
    display: flex;
    justify-items: contain;
    border-radius: 10px;
    overflow: hidden;
    margin-top: 10px;
`

export const LoginButtonText = styled.Text`
    background: #af52de;
    color: white;
    font-size: 18px;
    font-weight: bold;
    text-align: center;
    padding: 15px 0;
    
`