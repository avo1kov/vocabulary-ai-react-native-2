import React, { Component } from 'react'
import { AsyncStorage, View, Button } from 'react-native'
import { MySafeArea, WrapperScrollView, Lexeme, NewLexeme, WordTextInput, DefinitionTextInput, CollectionProps, CollectionEmoji, CollectionName } from './styles'
import { SERVER_URL } from 'react-native-dotenv'
import KeyboardSpacer from 'react-native-keyboard-spacer'

export class EditCollectionScreen extends Component {
    static navigationOptions = ({navigation}) => {
        const {params = {}} = navigation.state;
        return {
            headerTitle: 'Редактирование',
            headerRight: () => (
                <View>
                    <Button
                        onPress={() => params.sendCreationQuery()}
                        title={'Готово'}
                    />
                </View>
            ),
        };
    };

    constructor(props) {
        super(props);
        this.state = {
            lexemes: Array.from(Array(1), () => ({ token: '', definition: '' })), // [{ word: '', definition: '' }, { word: '', definition: '' }, { word: '', definition: '' }]// new Array(10).fill({ word: '', definition: '' })
            focusOnWord: true,
            collectionEmojiText: '',
            collectionNameText: '',
            wordAdded: false
        }
    }

    componentDidMount() {
        const collectionData = this.props.navigation.getParam('collectionData', null);
        if (collectionData) {
            this.setState({
                collectionId: collectionData.id,
                collectionEmojiText: collectionData.emoji,
                collectionNameText: collectionData.name,
                lexemes: collectionData.words,
            });
        }
        this.props.navigation.setParams({
            sendCreationQuery: this.sendCreationQuery
        });
    }

    getToken = async () => {
        try {
          const value = await AsyncStorage.getItem('@token')
          if(value !== null) {
            // value previously stored
            return value;
          }
          return null;
        } catch {
          // error reading value
        }
      }

    sendCreationQuery = () => {
        this.getToken().then(token => {
            fetch(`${SERVER_URL}/api/update_collection/`, {
                method: 'POST',
                headers: {
                    Authorization: token,
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    id: this.state.collectionId,
                    emoji: this.state.collectionEmojiText,
                    name: this.state.collectionNameText,
                    words: this.state.lexemes
                })
            })
                .then((response) => response.json())
                .then(() => {
                    this.props.navigation.navigate('Collections', { newCollectionData: {
                        emoji: this.state.collectionEmojiText,
                        name: this.state.collectionNameText,
                        words: this.state.lexemes
                    } });
                })
                .catch((error) => {
                    console.log("Error", `${SERVER_URL}/api/get_collections/`);
                });
        });
    }

    changeWord = (word, index) => {
        let lexemes = this.state.lexemes;
        lexemes[index].token = word;
        this.setState({ lexemes });
    }

    changeDefinition = (definition, index) => {
        let lexemes = this.state.lexemes;
        lexemes[index].definition = definition;
        this.setState({ lexemes });
    }

    addWord = from => {
        if (from == 'word') {
            this.setState({ focusOnWord: true });
        } else if (from == 'definition') {
            this.setState(() => ({ focusOnWord: false }));
        }
        this.setState(state => ({lexemes: [...state.lexemes, {token: '', definition: ''}], wordAdded: true}));
    }

    render() {
        return (
            <MySafeArea>
                <WrapperScrollView>
                    <CollectionProps>
                        <CollectionEmoji placeholder="👨‍🎓" onChangeText={ collectionEmojiText => this.setState({collectionEmojiText}) } value={this.state.collectionEmojiText}></CollectionEmoji>
                        <CollectionName placeholder="Имя набора" onChangeText={ collectionNameText => this.setState({collectionNameText}) } value={this.state.collectionNameText}></CollectionName>
                    </CollectionProps>
                    { this.state.lexemes.map((word, index) =>
                        <Lexeme key={index}>
                            <WordTextInput
                                placeholder="Слово"
                                value={word.token}
                                onChangeText={ word => this.changeWord(word, index) }
                                autoFocus={index + 1 == this.state.lexemes.length && (this.state.focusOnWord ? true : false) && this.state.wordAdded}
                            ></WordTextInput>
                            <DefinitionTextInput
                                placeholder="Значение"
                                value={word.definition}
                                onChangeText={ definition => this.changeDefinition(definition, index) }
                                autoFocus={index + 1 == this.state.lexemes.length && (!this.state.focusOnWord ? true : false) && this.state.wordAdded}
                            ></DefinitionTextInput>
                        </Lexeme>
                    ) }
                    <Lexeme>
                        <WordTextInput
                            placeholder="Слово"
                            onFocus={() => { this.addWord('word') }}
                        ></WordTextInput>
                        <DefinitionTextInput
                            placeholder="Значение"
                            onFocus={() => { this.addWord('definition') }}
                        ></DefinitionTextInput>
                    </Lexeme>
                </WrapperScrollView>
                <KeyboardSpacer />
            </MySafeArea>            
        )
    }
}

export default EditCollectionScreen