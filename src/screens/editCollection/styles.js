import styled from 'styled-components/native'
import { Dimensions } from 'react-native'

export const MySafeArea = styled.SafeAreaView`
    flex: 1;
`

export const WrapperScrollView = styled.ScrollView`
    flex: 1;
`

export const CollectionProps = styled.View`
    flex: 1;
    flex-direction: row;
    padding: 0 10px;
    padding-top: 7px;
    padding-bottom: 3px;
`

export const CollectionEmoji = styled.TextInput`
    background-color: #e5e5ea;
    margin: 3px 8px 3px 0;
    font-size: 24px;
    padding: 10px;
    border-radius: 10px;
    flex: 1;
`

export const CollectionName = styled.TextInput`
    background-color: #e5e5ea;
    margin: 3px 0px;
    font-size: 24px;
    padding: 10px;
    border-radius: 10px;
    flex: 10;
`

export const Lexeme = styled.View`
    padding: 3px 10px;
`

export const NewLexeme = styled.TouchableOpacity`
    padding: 3px 10px;
`
export const DefinitionTextInput = styled.TextInput`
    background-color: #e5e5ea;
    margin: 3px 0px;
    font-size: 24px;
    padding: 10px;
    border-radius: 10px;
    width: 100%;
`

export const WordTextInput = styled.TextInput`
    background-color: #e5e5ea;
    margin: 3px 0px;
    font-size: 24px;
    padding: 10px;
    border-radius: 10px;
    width: 100%;
`