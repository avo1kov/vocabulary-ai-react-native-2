import React from 'react'
import StartScreen from './src/screens/StartScreen/index.js'
import AuthScreen from './src/screens/authScreen'
import GameScreen from './src/screens/game/index.js'
import CollectionsScreen from './src/screens/collections/index.js'
import CreateCollectionScreen from './src/screens/createCollection'
import EditCollectionScreen from './src/screens/editCollection'

// import { render } from 'react-dom'
import { createStore } from 'redux'
import { Provider } from 'react-redux'
import rootReducer from './src/reducers/wordsSet'

import {createAppContainer} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';

const store = createStore(rootReducer)

const MainNavigator = createStackNavigator({
  StartScreen: { screen: StartScreen },
  AuthScreen: {
    screen: AuthScreen,
    navigationOptions: {
      headerShown: false,
    }
  },
  Collections: { screen: CollectionsScreen },
  CreateCollection: {screen: CreateCollectionScreen },
  EditCollection: {screen: EditCollectionScreen},
  Game: {screen: GameScreen},
});

const Navigation = createAppContainer(MainNavigator);

export default function App() {
  return (
    <Provider store={store}>
      <Navigation />
    </Provider>
  )
}